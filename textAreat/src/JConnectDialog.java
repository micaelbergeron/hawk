import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: micael
 * Date: 10/04/13
 * Time: 1:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class JConnectDialog extends JDialog {
    public enum Mode {
        HOST,
        JOIN
    }

    private JTextField txtHostname;
    private JTextField txtPort;
    private JLabel lblHostname;
    private JLabel lblPort;
    private JButton btnConnect;
    private JButton btnCancel;
    private boolean succeeded;

    public JConnectDialog(JFrame parent, Mode mode) {
        super(parent, "Host/Join game", true);

        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();

        cs.fill = GridBagConstraints.HORIZONTAL;

        if (mode == Mode.JOIN) {
            lblHostname = new JLabel("Hostname: ");
            cs.gridx = 0;
            cs.gridy = 0;
            cs.gridwidth = 1;
            panel.add(lblHostname, cs);

            txtHostname = new JTextField(30);
            cs.gridx = 1;
            cs.gridy = 0;
            cs.gridwidth = 2;
            panel.add(txtHostname, cs);
        }

        lblPort = new JLabel("Port: ");
        cs.gridx = 0;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panel.add(lblPort, cs);

        txtPort = new JTextField(5);
        cs.gridx = 1;
        cs.gridy = 1;
        cs.gridwidth = 2;
        panel.add(txtPort, cs);
        panel.setBorder(new LineBorder(Color.GRAY));

        if (mode == Mode.JOIN) {
            btnConnect = new JButton("Join");
        } else {
            btnConnect = new JButton("Host");
        }

        btnConnect.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                succeeded = true;
                dispose();
            }
        });
        btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        JPanel bp = new JPanel();
        bp.add(btnConnect);
        bp.add(btnCancel);

        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);

        pack();
        setResizable(false);
        setLocationRelativeTo(parent);
    }

    public String getHostname() {
        return txtHostname.getText().trim();
    }

    public int getPort() {
        return Integer.valueOf(txtPort.getText());
    }

    public boolean isSucceeded() {
        return succeeded;
    }
}
