import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

public class PlayerBoard extends Board 
{
    public PlayerBoard() {
        super();
    }

	public void PlaceBoatAtRandomPosition(ArrayList<Boat> boatList)
	{
		boolean boatPlaced = false;
		
		for (Boat boat : boatList)
		{
			while (!boatPlaced)
			{
				Random rdn1 = new Random();
				Random rdn2 = new Random();
				Random rdn3 = new Random();
				
				boatPlaced = BoatIsPlaced(new Position(rdn1.nextInt(9) + 'A', rdn2.nextInt(9)), boat,rdn3.nextInt(2));
			}
			boatPlaced = true;
		}
	}
	
	private boolean BoatIsPlaced (Position pos , Boat boat , int sens)
	{
		boolean boatPlaced = false;
		switch(sens)
		{
		case 0: // sens original horizontal
			if (pos.getIndex_Y() + boat.length <= 10)
			{
				if (!CheckPlaceForBoatAndPlaceIfPossible(pos, boat, 0))
				{
					if (pos.getIndex_X() + boat.length <= 10)
						boatPlaced = CheckPlaceForBoatAndPlaceIfPossible(pos, boat, 1);
					else
						boatPlaced = false;
				}
				else
					boatPlaced = true;
			}
			else
			{
				if (pos.getIndex_X() + boat.length <= 10)
					boatPlaced = CheckPlaceForBoatAndPlaceIfPossible(pos, boat, 1);
				else
					boatPlaced = false;
			}
			break;
			
		case 1: // vertical
			if (pos.getIndex_X() + boat.length <= 10)
			{
				if (!CheckPlaceForBoatAndPlaceIfPossible(pos, boat, 1))
				{
					if (pos.getIndex_Y() + boat.length <= 10)
						boatPlaced = CheckPlaceForBoatAndPlaceIfPossible(pos, boat, 0);
					else
						boatPlaced = false;
				}
				else
					boatPlaced = true;
			}
			else
			{
				if (pos.getIndex_Y() + boat.length <= 10)
					boatPlaced = CheckPlaceForBoatAndPlaceIfPossible(pos, boat, 0);
				else
					boatPlaced = false;
			}
			break;
		}
		return boatPlaced;
	}
	
	private boolean CheckPlaceForBoatAndPlaceIfPossible(Position pos , Boat boat , int sens)
	{
		boolean placeAvailable = true;
		boolean boatPlaced = false;
		switch (sens)
		{
			case 0:
			for (int col = pos.getIndex_Y() ; col <= (pos.getIndex_Y() + boat.length) ; col++)
			{
				if (grid[pos.getIndex_X()][col] != '_')
				{
					placeAvailable = false;
					break;
				}
			}
			
			if (placeAvailable)
			{
				boat.SetBegin(new Position(pos.getIndex_X(),pos.getIndex_Y()));
				boat.SetEnd(new Position(pos.getIndex_X(),(pos.getIndex_Y()+ boat.length)-1));
				
				for (int col = pos.getIndex_Y() ; col <= (pos.getIndex_Y() + boat.length) ; col++)
					grid[pos.getIndex_X()][col] = '+';
				
				boatPlaced=true;
			}
			break;
			
			case 1:
				for (int row = pos.getIndex_X() ; row <= (pos.getIndex_X() + boat.length) ; row++)
				{
					if (grid[row][pos.getIndex_Y()] != '_')
					{
						placeAvailable = false;
						break;
					}
				}
				
				if (placeAvailable)
				{
					boat.SetBegin(new Position(pos.getIndex_X(),pos.getIndex_Y()));
					boat.SetEnd(new Position((pos.getIndex_X() + boat.length)-1,pos.getIndex_Y()));
					
					for (int row = pos.getIndex_X() ; row <= (pos.getIndex_X() + boat.length) ; row++)
						grid[row][pos.getIndex_Y()] = '+';
					
					boatPlaced=true;
				}
				break;
		}
		return boatPlaced;
	}
	
	public HitType ShotAt(Position shotLocation)
	{
		int i_x = shotLocation.getIndex_X();
        int i_y = shotLocation.getIndex_Y();
		if (grid[i_x][i_y] == '_')
		{
			grid[i_x][i_y] = '*';
			return (HitType.MISS);
		}
		else
		{
			grid[i_x][i_y] = '#';
			return (HitType.HIT);
		}
	}
}
