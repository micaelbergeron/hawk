import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Application extends JPanel
{
    JFrame owner;
    protected JTextField textField;
    protected JTextArea chatArea,playerArea,opponentArea;
    protected JLabel playerTurnLabel;
    private final static String NEWLINE = System.getProperty("line.separator");
    Font gridFont = new Font(Font.MONOSPACED, Font.PLAIN, 16);

    Color turnColor = Color.green.darker();
    Color waitColor = Color.lightGray.brighter();

    BattleshipGame game = new BattleshipGame();
 
    public Application(JFrame owner)
    {
        super(new GridBagLayout());
        this.owner = owner;
        owner.setJMenuBar(createMenuBar());
        game.getClient().setClientListener(new Client.ClientListener()
        {
            @Override
            public void onUpdated() {
                updateBoards();
            }

            @Override
            public void onLog(String message) {
                chatArea.append(message);
                chatArea.append(NEWLINE);
            }

            @Override
            public void onStateChange(BattleshipProtocol.State oldState, BattleshipProtocol.State newState) {
                if (newState == BattleshipProtocol.State.WAITING_ACTION) {
                    playerArea.setBackground(turnColor);
                    opponentArea.setBackground(waitColor);
                } else if (newState == BattleshipProtocol.State.WAITING_REMOTE_ACTION) {
                    playerArea.setBackground(waitColor);
                    opponentArea.setBackground(turnColor);
                }
            }
        });

        playerTurnLabel = new JLabel("",JLabel.LEFT);
      
        playerTurnLabel.setPreferredSize(new Dimension(60,15));
        
        playerArea = new JTextArea(20,10);
        playerArea.setFont(gridFont);
        playerArea.setBackground(Color.LIGHT_GRAY);
        playerArea.setEditable(false);
        
        opponentArea = new JTextArea(20,10);
        opponentArea.setFont(gridFont);
        opponentArea.setBackground(Color.LIGHT_GRAY);
        opponentArea.setEditable(false);
        
        JSplitPane upSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,playerArea,opponentArea);
        upSplitPane.setOneTouchExpandable(true);
        upSplitPane.setResizeWeight(0.5);
        
        chatArea = new JTextArea(10,20);
        chatArea.setBackground(new Color(255,255,255));
        chatArea.setEditable(false);
        
        textField = new JTextField(20);
        textField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String text = textField.getText();
                game.handle(text);

                chatArea.append(textField.getText() + NEWLINE);
                textField.setText("");
                chatArea.setCaretPosition(chatArea.getDocument().getLength());
            }
        });

        /*
        JScrollPane BoardPane = new JScrollPane(boardArea);
        BoardPane.setPreferredSize(new Dimension(800,600));
        BoardPane.setMinimumSize(new Dimension(800,500));
        */
 
        JScrollPane ChatPane = new JScrollPane(chatArea);
        ChatPane.setPreferredSize(new Dimension(800,80));
        ChatPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        ChatPane.setMaximumSize(new Dimension(800,10));
        
        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,upSplitPane,ChatPane);
        splitPane.setOneTouchExpandable(true);
        splitPane.setResizeWeight(0.5);
        splitPane.setEnabled(false);
        
        //Add Components to this panel.
        GridBagConstraints c = new GridBagConstraints();
        c.gridwidth = GridBagConstraints.REMAINDER;
 
        add(playerTurnLabel,c);
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0;
        c.weighty = 1.0;
        
        add(splitPane,c);
        
        //c.fill = GridBagConstraints.BOTH;
        add(textField, c);
        updateBoards();
    }

    JMenuBar createMenuBar()
    {
        JMenuBar menuBar = new JMenuBar();
        JMenu menuGame = new JMenu("Game");

        JMenuItem menuItemHost = new JMenuItem("Host game...");
        menuItemHost.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JConnectDialog connectDialog = new JConnectDialog(owner, JConnectDialog.Mode.HOST);
                connectDialog.setVisible(true);
                if (connectDialog.isSucceeded()) {
                    game.setPort(connectDialog.getPort());
                    game.host();
                }
            }
        });

        JMenuItem menuItemJoin = new JMenuItem("Join game...");
        menuItemJoin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JConnectDialog connectDialog = new JConnectDialog(owner, JConnectDialog.Mode.JOIN);
                connectDialog.setVisible(true);
                if (connectDialog.isSucceeded()) {
                    game.setPort(connectDialog.getPort());
                    game.setServer_name(connectDialog.getHostname());
                    game.join();
                }
            }
        });

        menuGame.add(menuItemHost);
        menuGame.add(menuItemJoin);
        menuBar.add(menuGame);
        return menuBar;
    }

    void updateBoards() {
        opponentArea.setText(game.getOpponentBoard().toDisplay());
        playerArea.setText(game.getPlayerBoard().toDisplay());
    }

    void joinDefaultGame() {
        game.setServer_name("localhost");
        game.setPort(8888);
        game.join();
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event dispatch thread.
     */
    private static void createAndShowGUI() 
    {
        //Create and set up the window.
        JFrame frame = new JFrame("BattleShip");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
        //Add contents to the window.
        frame.add(new Application(frame));
        //Display the window.
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);
    }
 
    public static void main(String[] args) {

        javax.swing.SwingUtilities.invokeLater(new Runnable() 
        {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}

