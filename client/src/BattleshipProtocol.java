/**
 * Created with IntelliJ IDEA.
 * User: micae_000
 * Date: 13-04-07
 * Time: 14:17
 * To change this template use File | Settings | File Templates.
 */
public class BattleshipProtocol {

    public enum State {
        WAITING_ACTION,
        WAITING_REMOTE_ACTION,
        WAITING_RESPONSE
    }

    public BattleshipProtocol() {
        this.state = State.WAITING_ACTION;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        if (debug) {
            System.out.println("Protocol changing state to " + state.name());
        }
        this.state = state;
    }

    final static boolean debug = true;
    final static String POSITION_REGXP = "^[A-L][0-9]$";
    final static String MESSSAGE_REGXP = "^#.*$";

    State state;

    // position format: XY [0-9A-L]{2}
    public static boolean isPosition(String data) {
        if (data.matches(POSITION_REGXP)) {
            return true;
        }
        return false;
    }

    // message format: #<message>
    public static boolean isChat(String data) {
        if (data.matches(MESSSAGE_REGXP)) {
            return true;
        }
        return false;
    }

    public static boolean isHit(String data) {
        for (HitType hit_t : HitType.values()) {
            if (hit_t.name().equals(data)) {
                return true;
            }
        }
        return false;
    }


    public boolean canSendPosition() { return state == State.WAITING_ACTION; }
    public boolean canReceiveHit() { return state == State.WAITING_RESPONSE; }
    public boolean canReceivePosition() { return state == State.WAITING_REMOTE_ACTION; }
}
