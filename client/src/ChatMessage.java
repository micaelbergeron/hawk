import java.io.Serializable;
import java.sql.Time;

/**
 * Created with IntelliJ IDEA.
 * User: micae_000
 * Date: 13-04-07
 * Time: 14:41
 * To change this template use File | Settings | File Templates.
 */
public class ChatMessage implements Serializable {
    String message;
    String sender;
    Time timestamp;

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Time getTimestamp() {

        return timestamp;
    }

    public void setTimestamp(Time timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {

        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String Serialize() {
        final StringBuffer sb = new StringBuffer();
        sb.append("#" + getMessage());
        return sb.toString();
    }

    public static ChatMessage Deserialize(String data) {
        ChatMessage out_message = new ChatMessage();
        String msg = data.replaceFirst("#\\s*", "");
        out_message.setMessage(msg);
        return out_message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChatMessage that = (ChatMessage) o;

        if (message != null ? !message.equals(that.message) : that.message != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return message != null ? message.hashCode() : 0;
    }
}
