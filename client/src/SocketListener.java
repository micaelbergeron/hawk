import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: micae_000
 * Date: 13-04-07
 * Time: 13:08
 * To change this template use File | Settings | File Templates.
 */
public class SocketListener extends Thread {

    static final int POLLING_WAIT = 100;
    public boolean debug = true;
    boolean canRead = true;

    public interface OnDataReceivedListener {
        void onDataReceived(String data);
    }
    OnDataReceivedListener listener;

    BufferedReader reader;

    public SocketListener(InputStream input) {
        super("SocketListener");
        this.reader = new BufferedReader(new InputStreamReader(input));
    }

    public void setOnDataReceivedListener(OnDataReceivedListener listener) {
        this.listener = listener;
    }

    @Override
    public void run() {
        String read;
        while(canRead) {
            try {
                if (debug) {
                    System.out.println("Polling...");
                }
                String data = reader.readLine();
                if (listener != null) {
                    listener.onDataReceived(data);
                }
            } catch (IOException e) {
                canRead = false;
                System.out.println("Connection broke.");
                break;
            }
            yield();
        }
        System.out.println("SocketListener stopped.");
    }
}
