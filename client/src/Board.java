
public class Board 
{
    private static final String NEWLINE = System.getProperty("line.separator");
	protected char[][] grid;
	
	  Board()
	  {
		  grid = new char[10][10];
		  InitializeBoard();
	  }
	  
	  public void InitializeBoard()
	  {
		  for (int row = 0 ; row < 10 ; row++)
		  {
			  for (int column = 0 ; column < 10 ; column++)
				  grid[row][column] = '_';
		  }
	  }
	  
	  public void UpdateGridWhitHitInfo (Position pos, HitType type)
	  {
		  switch (type)
		  {
		  	case MISS:
		  		grid[pos.getIndex_X()][pos.getIndex_Y()] = '*';
		  		break;
		  		
		  	case HIT:
		  		grid[pos.getIndex_X()][pos.getIndex_Y()] = '#';
		  		break;
		  }
	  }
	  
	  public Position ShotLocationToPosition(String ShotLocation)
	  {
		  char caracterLine = ShotLocation.charAt(0);
		  caracterLine = Character.toUpperCase(caracterLine);
		  int column = Integer.parseInt(ShotLocation.substring(1));
		  Position position;
		  
		  switch (caracterLine)
		  {
		  	case 'A':
		  		position = new Position(0, column);
		  		break;
		 	case 'B':
		  		position = new Position(1, column);
		  		break;
		 	case 'C':
		  		position = new Position(2, column);
		  		break;
		 	case 'D':
		  		position = new Position(3, column);
		  		break; 	
		  	case 'E':
			  	position = new Position(4, column);
			  	break;
		 	case 'F':
		  		position = new Position(5, column);
		  		break;
		 	case 'G':
		  		position = new Position(6, column);
		  		break;
		 	case 'H':
		  		position = new Position(7, column);
		  		break;
		 	case 'I':
		  		position = new Position(8, column);
		  		break;
		 	case 'J':
		  		position = new Position(9, column);
		  		break;
		 	default:
		 		position = new Position(-1, -1);
		 		break;
		  }
		  return position;  
	  }
	  
	public String toDisplay()
    {
        StringBuilder stringBuilder = new StringBuilder();
        // header for columns
        stringBuilder.append("  "); // 2 spaces
        for (int i = 0; i < 10; i++) {
            stringBuilder.append(String.valueOf(i));
            stringBuilder.append(' ');
        }
        stringBuilder.append(NEWLINE);
        for (int i = 0; i < 10; i++) {
            stringBuilder.append((char)('A' + i));
            stringBuilder.append(' ');
            for (int j = 0; j < 10; j++) {
                stringBuilder.append(grid[i][j]);
                stringBuilder.append(' ');
            }
            stringBuilder.append(NEWLINE);
        }
        return stringBuilder.toString();
    }

}
