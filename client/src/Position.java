public class Position {
	int X,Y;
	Position (int x,int y){X=x;Y=y;}

    public Position() {
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("Position");
        sb.append("{X=").append((char)X);
        sb.append(", Y=").append(Y);
        sb.append('}');
        return sb.toString();
    }

    public int getIndex_X() { return X - 'A'; }
    public int getIndex_Y() { return Y; }

    // a really dummy serialization, just enough for this work
    public String Serialize() {
        final StringBuffer sb = new StringBuffer();
        sb.append((char)X)
          .append(Y);
        return sb.toString();
    }

    public static Position Deserialize(String data) {
        Position out_pos = new Position();
        out_pos.X = data.charAt(0);
        out_pos.Y = data.codePointAt(1) - '0';
        return out_pos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        if (X != position.X) return false;
        if (Y != position.Y) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = X;
        result = 31 * result + Y;
        return result;
    }
}
	
