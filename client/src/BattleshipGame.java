import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: micae_000
 * Date: 13-04-07
 * Time: 15:43
 * To change this template use File | Settings | File Templates.
 */
public class BattleshipGame
{
	public ArrayList<Boat>boatList;
	public Board opponentBoard;
	public PlayerBoard playerBoard;
    protected String playerName;
    int port;
    String server_name;
    boolean quit = false;
    public int score;

    Client client;

    public BattleshipGame()
	{
         client = new Client(this);
		 boatList  = CreateBoats();
		 opponentBoard = new Board();
		 playerBoard = new PlayerBoard(); // with no boats;
		 PlaceBoats();
	}
	
	public int getPort() 
    {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getServer_name() {
        return server_name;
    }

    public void setServer_name(String server_name) {
        this.server_name = server_name;
    }

    public synchronized PlayerBoard getPlayerBoard() {
        return playerBoard;
    }

    public synchronized Board getOpponentBoard() {
        return opponentBoard;
    }

    public Client getClient() {
        return client;
    }

    public void host() {
        try {
            Socket connection = null;
            client.setProtocolState(BattleshipProtocol.State.WAITING_REMOTE_ACTION);

            ServerSocket server = new ServerSocket(port);

            System.out.println("Waiting " + server.getLocalPort()+"...");
            connection = server.accept();
            System.out.println("Just connect to " + connection.getRemoteSocketAddress());

            client.connect(connection);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void join() {
        try
        {
            client.connect(server_name, port);
            client.setProtocolState(BattleshipProtocol.State.WAITING_ACTION);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    
    ArrayList<Boat> CreateBoats()
    {
    	ArrayList<Boat> BoatList = new ArrayList<Boat>();
    	BoatList.add(new Boat(1));
    	BoatList.add(new Boat(2));
    	BoatList.add(new Boat(3));
    	BoatList.add(new Boat(4));
    	BoatList.add(new Boat(5));
    	
    	return BoatList;
    }
    
    public void PlaceBoats()
    {    
    	playerBoard.PlaceBoatAtRandomPosition(boatList);
    }
    
    void handle(String data) {
        if (client.isConnected()) {
            if (BattleshipProtocol.isChat(data)) {
                client.send(ChatMessage.Deserialize(data));
            } else if (BattleshipProtocol.isPosition(data)) {
                client.send(Position.Deserialize(data));
            } else {
                System.out.println("To chat, use the # before your message.");
            }
        }
    }

    public void close()
    {
        client.close();
    }

    public static void main(String[] arg) {
        BattleshipGame game = new BattleshipGame();

        if (arg.length == 2) // client
        {
            game.setServer_name(arg[0]);
            game.setPort(Integer.parseInt(arg[1]));
            game.join();
        } else if (arg.length == 1) { // server
            game.setPort(Integer.parseInt(arg[0]));
            game.host();
        } else {
            System.out.println("Missing arguments [hostname] port");
        }

        BufferedReader sysin = new BufferedReader(new InputStreamReader(System.in));
        while(!game.quit) {
            try {
                String data = sysin.readLine();
                game.handle(data);
            } catch (IOException e) {
                game.quit = true;
            }
        }
    }
}
