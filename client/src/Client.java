import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;

public class Client implements SocketListener.OnDataReceivedListener
{
    public interface ClientListener
    {
        void onUpdated();
        void onLog(String message);
        void onStateChange(BattleshipProtocol.State oldState, BattleshipProtocol.State newState);
    }
    ClientListener clientListener;
    void onUpdate() { if (clientListener != null) clientListener.onUpdated(); }
    public void setClientListener(ClientListener listener) {
        this.clientListener = listener;
    }

    List<Position> shots = new ArrayList<Position>();

    String host_name;
    int port;
    Socket connection;
    boolean connected = false;
    SocketListener listener;

    BattleshipProtocol protocol = new BattleshipProtocol();
    BattleshipGame game;

    public Client(BattleshipGame game) {
        this.game = game;
        connection = new Socket();
    }

    public void setProtocolState(BattleshipProtocol.State state) {
        BattleshipProtocol.State oldState = protocol.getState();
        this.protocol.setState(state);
        if (clientListener != null) {
            clientListener.onStateChange(oldState, state);
        }
    }

    public void log(String message) {
        System.out.println(message);
        if (clientListener != null) {
            clientListener.onLog(message);
        }
    }

    public void connect(String host_name, int port) {
        // will try to connect n times, at 2^5+n ms interval
        SocketAddress remote_addr = new InetSocketAddress(host_name, port);
        int i = 0;
        while (!isConnected())
        {
            try
            {
                int timeout = (int)Math.pow(2, 5+i);
                log("Connecting to " + host_name + ":" +  String.valueOf(port));
                connection.connect(remote_addr, timeout);
                log("Just connect to " + connection.getRemoteSocketAddress());

                connect(connection);
            }
            catch (IOException e)
            {
                e.printStackTrace();
                this.close();
                log("Remote disconnected.");
                break;
            }
        }
    }

    public void connect(Socket s ) throws IOException {
        if (s.isConnected()) {
            this.connection = s;
        }

        listener = new SocketListener(connection.getInputStream());
        listener.setOnDataReceivedListener(this);
        listener.start();
    }

    public boolean isConnected() { return connection.isConnected(); }

    public void send(Position p) {
        if (isConnected()) {
            if (protocol.canSendPosition()) {
                if (!shots.contains(p)) {
                    shots.add(p);
                    send(p.Serialize());
                    protocol.setState(BattleshipProtocol.State.WAITING_RESPONSE);
                } else {
                    log("You already shot at " + p.Serialize());
                }
            } else {
                log("It's not your turn.");
            }
        }
    }

    public void send(ChatMessage msg) {
        if (isConnected()) {
            send(msg.Serialize());
        }
    }

    public void send(HitType hitType) {
        if (isConnected()) {
            send(hitType.name());
        }
    }

    private void send(String data) {
        if (isConnected()) {
            try {
                OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
                osw.write(data);
                osw.write(13);
                osw.flush();
            } catch (IOException e) {
                log("ERROR! Could not send data: " + e.getMessage());
            }
        }
    }

    public void close() {
        try {
            connection.close();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Override
    public void onDataReceived(String data) {
        // check if data is valid
        boolean triggerUpdate = true;
        if (protocol.canReceivePosition() && BattleshipProtocol.isPosition(data)) {
            Position p = Position.Deserialize(data);
            HitType hit = game.getPlayerBoard().ShotAt(p);
            log("Position : " + p.toString() + " => " + hit.name());
            send(hit);

            setProtocolState(BattleshipProtocol.State.WAITING_ACTION);
        } else if (BattleshipProtocol.isChat(data)) {

            ChatMessage chat = ChatMessage.Deserialize(data);
            log("Chat: " + chat.getMessage());

        } else if (protocol.canReceiveHit() && BattleshipProtocol.isHit(data)) {

            HitType hit = HitType.valueOf(data);
            Position lastShot = shots.get(shots.size()-1);
            log("Hit: " + hit.name() + " at " + lastShot.toString());
            game.getOpponentBoard().UpdateGridWhitHitInfo(lastShot, hit);

            if (hit == HitType.SINK) {
                game.score += 1;
            }

            setProtocolState(BattleshipProtocol.State.WAITING_REMOTE_ACTION);
        } else {
            triggerUpdate = false;
            log("Undefined data: " + data);
        }

        if (triggerUpdate) onUpdate();
    }
}
